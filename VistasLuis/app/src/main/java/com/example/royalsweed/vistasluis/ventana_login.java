package com.example.royalsweed.vistasluis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ventana_login extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_login);
    }

    public void lanzar(View view)
    {
        Intent i = new Intent(this, Dashboard.class);
        startActivity(i);
    }
}
