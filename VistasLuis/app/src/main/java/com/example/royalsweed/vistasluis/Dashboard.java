package com.example.royalsweed.vistasluis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Dashboard extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_dashboard);
    }
    public void profesores(View view)
    {
        Intent i = new Intent(this, Profesores.class);
        startActivity(i);
    }
    public void administradores(View view)
    {
        Intent i = new Intent(this, Administradores.class);
        startActivity(i);
    }
    public void alumnos(View view)
    {
        Intent i = new Intent(this, alumnos_tablas.class);
        startActivity(i);
    }
    public void salir(View view)
    {
        Intent i = new Intent(this, ventana_login.class);
        startActivity(i);
    }
}
