package com.example.royalsweed.vistasluis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Alumnos extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_agregar_alumnos);
    }
    public void atras(View view)
    {
        Intent i = new Intent(this, alumnos_tablas.class);
        startActivity(i);
    }
    public void inicio(View view)
    {
        Intent i = new Intent(this, Dashboard.class);
        startActivity(i);
    }
}
