package com.example.royalsweed.vistasluis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Agregar_admin extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_agregar_admin);
    }
    public void atras(View view)
    {
        Intent i = new Intent(this, Administradores.class);
        startActivity(i);
    }
    public void inicio(View view)
    {
        Intent i = new Intent(this, Dashboard.class);
        startActivity(i);
    }
}
