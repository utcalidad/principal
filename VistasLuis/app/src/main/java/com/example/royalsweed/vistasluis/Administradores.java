package com.example.royalsweed.vistasluis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Administradores extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_administradores);
    }
    public void agregarAdmin(View view)
    {
        Intent i = new Intent(this, Agregar_admin.class);
        startActivity(i);
    }
}
